import React, { useEffect, useContext } from 'react';
import { View, Text } from 'react-native';
import { useSelector } from 'react-redux';
import UserContext from '../contexts/userContext';
export default function LifeCycleTest({name}) {
    const user = useContext(UserContext);
    const userSelector = useSelector(state => state.user.name)
    useEffect(() => {
        return () => {
            
        }
    }, []);
    return (
        <View>
            <Text>{`props: ${name}-context: ${user}-redux: ${userSelector}`}</Text>
        </View>
    )
}