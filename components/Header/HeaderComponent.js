import React from 'react'
import { ThemeProvider, Header } from 'react-native-elements';
import LeftComponent from './LeftComponent'
import RightComponent from './RightComponent'
import CenterComponent from './CenterComponent'
export default function HomeScreen() {
    return (
        <ThemeProvider>
            <Header
              leftComponent={<LeftComponent />}
              centerComponent={<CenterComponent />}
              rightComponent={<RightComponent />}
              backgroundColor='#ee5e9c'
            />
        </ThemeProvider>
    )
}