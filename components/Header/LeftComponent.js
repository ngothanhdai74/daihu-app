import React, { useState, useEffect } from 'react'
import { Icon } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';

export default function LeftComponent() {
    const navigation = useNavigation();

    const toogleMenu = () => {
        navigation.openDrawer()
    }

    return (
            <Icon
                name='menu'
                color='#fff'
                onPress={toogleMenu}
                size={24}
            />
    )
}