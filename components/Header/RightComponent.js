import React, { useState, useEffect } from 'react'
import { Icon } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { View } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
export default function RightComponent() {
    const navigation = useNavigation();

    const toogleMenu = () => {
        navigation.openDrawer()
    }

    return (
        <View style={{flex: 1, flexDirection: 'row'}}>
            <View>
            <Icon
                    name='notifications'
                    color='#fff'
                />
            </View>
            <View style={{width: 10, height: 1, backgroundColor: '#ee5e9c'}} />
            <View>
            <FontAwesome name="user-circle-o" size={24} color="#fff" onPress={toogleMenu}/>
                
            </View>

                
        </View>
    )
}