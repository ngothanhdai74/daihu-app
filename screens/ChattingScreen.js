import { Text, View, Button  } from 'react-native'
import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import LifeCycleTest from '../components/LifeCycleTest'
import {UserProvider} from '../contexts/userContext'

export default function HomeScreen({ navigation }) {
    const [count1, setCount1] = useState(0);
    const [count2, setCount2] = useState(10);
    const [fullName, setFullName] = useState('đại');
    const dispatch = useDispatch()
    const incrementHandler1 = async () => {
        dispatch({
            type: 'user/add',
            payload: 'đại'
        })
        setCount1(count1 + 1)
    }
    const incrementHandler2 = async () => {
        setCount2(count2 - 1)
    }

    useEffect(() => {
    }, [count1]);

    useEffect(() => {
    }, [count2]);

    useEffect(() => {
    }, []);


    return (
        <UserProvider value={fullName}>
            <View>
                <LifeCycleTest name='Con nè cha'></LifeCycleTest>
                
                <Button onPress={incrementHandler1} title="Tăng lên đê" />
                <Button onPress={incrementHandler2} title="Giảm xuống" />

                <Button onPress={() => {
                    navigation.navigate('Dashboard')
                }} title="Trang Dashboard" />
            </View>
        </UserProvider>
    )
}