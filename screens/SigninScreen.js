import React, { useState, useEffect } from 'react'
import { Platform, Text, View, StyleSheet, ScrollView, SectionList, Image, TextInput, Button, FlatList } from 'react-native'

export default function DashboardScreen({ navigation }) {

    const [isHungry, setIsHungry] = useState(true);
    const dataList = [
        {key: 'Đại 🎉'},
        {key: 'Huyền 🍕'},
        {key: Platform.OS },
        {key: Platform.Version},
        
        ]
    const [text, setText] = useState('');

    useEffect(() => {

    },[])

    return (
        <View style={styles.toCenter}>
          <Image
              source={{
                  uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
              }}
              style={{ width: 200, height: 200 }}
          />
          <Button onPress={() => {
                    navigation.push('Home')
                }} title="Về trang chủ" />
          <Button onPress={() => {
              navigation.goBack()
          }} title="Về trước" />
          <TextInput placeholder="nhập gì cũng được!" style={styles.customInput} />
          <Button
              onPress={() => {
              setIsHungry(false);
              }}
              disabled={!isHungry}
              onChangeText={text => {
                setText(text)
              }}
              title={isHungry ? "Cho tao ăn! 🍕" : "ok được rồi!"}
          />
          <FlatList data={dataList} renderItem={({item}) => <Text key={item.key}>{item.key}</Text>}/>
          <ScrollView>
              <SectionList
              sections={[
                  {title: 'Tài khoản', data: ['Đăng xuất', 'Thông tin cá nhân', 'Thu nhập']},
                  {title: 'Vị trí', data: ['Địa điểm gần nhất']},
              ]}
              renderItem={({item}) => <Text style={style1.item}>{item}</Text>}

              renderSectionHeader={({section}) => <Text style={style1.sectionHeader}>{section.title}</Text>}

              keyExtractor={(item, index) => index}
              />
          </ScrollView>


        </View>
    )
}

const styles = StyleSheet.create({
  toCenter: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  customInput: {
    height: 40,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1
  }
});
const style1 = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    sectionHeader: {
      paddingTop: 2,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 2,
      fontSize: 14,
      fontWeight: 'bold',
      backgroundColor: 'rgba(247,247,247,1.0)',
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  })
  