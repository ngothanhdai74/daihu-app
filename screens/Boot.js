import React from 'react'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import RootStackNavigator from '../navigators/RootStackNavigator'
export default function Boot() {
    return (
      <NavigationContainer>
        <RootStackNavigator />
      </NavigationContainer>
    ) 
}