import React, { useState, useEffect } from 'react'
import { Platform, Text, View, StyleSheet, ScrollView, SectionList, Image, TextInput, Button, FlatList } from 'react-native'
import { Overlay } from 'react-native-elements';

import HttpFactory from '../httpFactory'
import { ListItem, Avatar } from 'react-native-elements'


export default function DashboardScreen({ navigation }) {

    const [isHungry, setIsHungry] = useState(true);
    const dataList = [
        {key: 'Đại 🎉'},
        {key: 'Huyền 🍕'},
        {key: Platform.OS },
        {key: Platform.Version},
        
        ]
    const [text, setText] = useState('');
    
    const erpApi = HttpFactory.getErpApi();
    const [users, setUsers] = useState([])
    useEffect(() => {
      (async () => {
        let { data }  = await erpApi.get('/api/users?page=2')
        console.log(data[0])
        if(users) {
          setUsers([data[0]])
        }
      })()
    },[])

    const [visible, setVisible] = useState(false);

    const toggleOverlay = () => {
      setVisible(!visible);
    };
  
    return (
        <View style={styles.toCenter}>
          <Image
              source={{
                  uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
              }}
              style={{ width: 200, height: 200 }}
          />
          {
            users.map((l, i) => (
              <ListItem key={i} bottomDivider>
                <Avatar source={{uri: l.avatar}} />
                <Text>{l.first_name + ' ' + l.last_name}</Text>
              </ListItem>
            ))
          }
          <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>

          </Overlay>

          <Button onPress={toggleOverlay} title="xem model" />
          <Button onPress={() => {
              navigation.goBack()
          }} title="Về trước" />
          <TextInput placeholder="nhập gì cũng được!" style={styles.customInput} />
          <Button
              onPress={() => {
              setIsHungry(false);
              }}
              disabled={!isHungry}
              onChangeText={text => {
                setText(text)
              }}
              title={isHungry ? "Cho tao ăn! 🍕" : "ok được rồi!"}
          />
          <FlatList data={dataList} renderItem={({item}) => <Text key={item.key}>{item.key}</Text>}/>
          <ScrollView>
              <SectionList
              sections={[
                  {title: 'Tài khoản', data: ['Đăng xuất', 'Thông tin cá nhân', 'Thu nhập']},
                  {title: 'Vị trí', data: ['Địa điểm gần nhất']},
              ]}
              renderItem={({item}) => <Text style={style1.item}>{item}</Text>}

              renderSectionHeader={({section}) => <Text style={style1.sectionHeader}>{section.title}</Text>}

              keyExtractor={(item, index) => index}
              />
          </ScrollView>


        </View>
    )
}

const styles = StyleSheet.create({
  toCenter: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  customInput: {
    height: 40,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1
  }
});
const style1 = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    sectionHeader: {
      paddingTop: 2,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 2,
      fontSize: 14,
      fontWeight: 'bold',
      backgroundColor: 'rgba(247,247,247,1.0)',
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  })
  