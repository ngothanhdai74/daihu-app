import React, { useState, useEffect } from 'react'
import Header from '../components/Header/HeaderComponent'
import { View, Text } from 'react-native'
import config from '../configs'
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function HomeScreen() {
    
    const [accessToken, setAccessToken] = useState('vl')
    useEffect(() => {
        (async () => {
            await AsyncStorage.setItem('access_token', 'đại trai đẹp')

            const data = await AsyncStorage.getItem('access_token')
            if(data){
                setAccessToken(data)
            }
        })()
    }, [])

    return (
        <View>
            <Header />
            <Text>{config.API_KEY}</Text>
            <Text>{config.ERP_API_URL}</Text>
            <Text>{accessToken}</Text>
        </View>
    )
}