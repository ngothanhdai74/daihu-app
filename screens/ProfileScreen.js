import { Text, View, StyleSheet, Button, Platform  } from 'react-native'
import React, { useState, useEffect } from 'react'
import { Container } from 'native-base'
import DateTimePicker from '@react-native-community/datetimepicker';

export default function ProfileScreen() {

  const [date, setDate] = useState(new Date(1598051730000));

  const [mode, setMode] = useState('date');

  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {

    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    
  };
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };


    return (
      <View style={styles.toCenter}>
      <View>
        <Button onPress={showDatepicker} title="Chọn ngày" />
      </View>
      <View>
        <Button onPress={showTimepicker} title="Chọn giờ" />
      </View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
      <View>
        <Text>{date.toDateString()}</Text>
      </View>
    </View>
    )
}

const styles = StyleSheet.create({
    toCenter: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    }
  });