import { API_KEY, ERP_API_URL } from '@env'

export default {
    API_KEY,
    ERP_API_URL
}