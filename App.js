import React,{ useState, useEffect } from 'react';
import { Provider } from 'react-redux';
import store from './utils/store'
import Boot from './screens/Boot'
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App() {
  const [isReady, setIsReady] = useState(false)
  useEffect(() => {
    (async () => {
      await Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      });
      setIsReady(!isReady)
    })()
  }, [])

  return (
    <Provider store={store}>
      <SafeAreaProvider>
        {isReady && <Boot />}
      </SafeAreaProvider>
    </Provider>
  );
}