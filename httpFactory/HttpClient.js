import axios from 'axios'

export default class HttpClient {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }
    async get(url) {
        try {
            let fullUrl = this.baseUrl + url;
            const { data: response } = await axios.get(fullUrl);
            return response;
        } catch (error) {
            console.log(error)
        }
    }
    async post(url, request) {
        try {
            let fullUrl = this.baseUrl + url;
            const { data: response } = await axios.post(fullUrl, request);
            return response;
        } catch (error) {
            console.log(error)
        }
    }
    async delete(url, request) {
        try {
            let fullUrl = this.baseUrl + url;
            const { data: response } = await axios.delete(fullUrl, request);
            return response;
        } catch (error) {
            console.log(error)
        }
    }
    async put(url, request) {
        try {
            let fullUrl = this.baseUrl + url;
            const { data: response } = await axios.put(fullUrl, request);
            return response;
        } catch (error) {
            console.log(error)
        }
    }
    async send(url = '', method = 'get', body = {}){
        try {
            let config = {
                url: url,
                method: method,
                baseURL: this.baseUrl,
                data: body,
              };
            const { data: response } = await axios(config);
            return response;
        } catch (error) {
            console.log(error)
        }
    }
    async uploadFile(url, formData){
        try {
            let fullUrl = this.baseUrl + url;
            const { data: response } = await axios.post(fullUrl, formData, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              }
            });
            return response;
        } catch (error) {
            console.log(error)
        }
    }


}