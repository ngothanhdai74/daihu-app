import HttpClient from './HttpClient'
export default class HttpFactory {
    static getErpApi() {
        return new HttpClient('https://reqres.in')
    }
}