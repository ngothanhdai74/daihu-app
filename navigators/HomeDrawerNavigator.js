import React from 'react'
import RootTabNavigator from './RootTabNavigator'
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>

      <DrawerItemList {...props} />

      <DrawerItem
        label="Thoát"
        onPress={() => props.navigation.closeDrawer()}
      />

      <DrawerItem
        label="Out"
        onPress={() => props.navigation.toggleDrawer()}
      />
      
    </DrawerContentScrollView>
  );
}
const Drawer = createDrawerNavigator();


export default function HomeDrawerNavigator() {
    return (
        <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
          <Drawer.Screen name="RootTabNavigator" component={RootTabNavigator} />
        </Drawer.Navigator>
    ) 
}