import React from 'react'
import DashboardScreen from '../screens/DashboardScreen'
import SettingScreen from '../screens/SettingScreen'
import StatisticRevenueScreen from '../screens/StatisticRevenueScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons, AntDesign } from '@expo/vector-icons';
import HomeScreen from '../screens/HomeScreen'

const Tab = createBottomTabNavigator();

export default function RootTabNavigation() {

    return (
        <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            switch(route.name) {
              case 'Home': {
                return <Ionicons name="home" size={size} color={color} />
              }
              case 'Dashboard': {
                return <AntDesign name="dashboard" size={size} color={color} />
              }
              case 'Statistic': {
                return <Ionicons name='podium' size={size} color={color} />;
              }
              case 'Setting': {
                return <Ionicons name='settings' size={size} color={color} />;
              }
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: '#ee5e9c',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Home" component={HomeScreen} options={{ title: 'Trang chủ' }}/>
        <Tab.Screen name="Dashboard" component={DashboardScreen} options={{ title: 'Tổng quan' }}/>
        <Tab.Screen name="Statistic" component={StatisticRevenueScreen} options={{ title: 'Thông kê' }}/>
        <Tab.Screen name="Setting" component={SettingScreen} options={{ title: 'Cài đặt' }}/>
      </Tab.Navigator>
    ) 
}