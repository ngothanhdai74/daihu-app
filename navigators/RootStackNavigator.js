import React from 'react'
import SigninScreen from '../screens/SigninScreen'
import HomeDrawerNavigator from './HomeDrawerNavigator'
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

export default function RootStackNavigation() {
    return (
      <Stack.Navigator headerMode='none'>
        <Stack.Screen 
          name="Main" 
          component={HomeDrawerNavigator}/>
        <Stack.Screen
          name="Signin" 
          component={SigninScreen}/>
      </Stack.Navigator>
    )
}