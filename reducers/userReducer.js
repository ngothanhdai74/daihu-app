const initialState = {
    name: '',
    age: '',
    roles: []
}

const userReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'user/add': {
            
            return {
                ...state,
                name: action.payload
            }
        }
        case 'user/update': {

            return state
        }
        case 'user/delete': {

            return state
        }
        case 'user/getById': {

            return state
        }
        case 'user/getAll': {

            return state
        }
        default: {
            return state
        }
    }
}

export default userReducer